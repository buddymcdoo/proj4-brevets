# Project 4: Brevet time calculator with Ajax
Author: Simon Ward
Contact: simonward3000@gmail.com

#Definintion and Purpose:
This project is a time calculator for brevets, used in Randonneurs bicycle races.
Taken from saltlakerandos.org: 
	"A Brevet is a timed, long distance road cycling event. 
	Brevet (bruh vay) means certificate which refers to the 
	card carried by randonneurs which gets stamped or signed 
	at checkpoints along the way."
This calcultor, based off of the one from the Randonneurs website (https://rusa.org/octime_acp.html),
allows the user to enter distances (where these brevet checkpoints will be located), and, based off of
an official set of rules, calculates the time frame that the cyclist is allowed to arrive at the checkpoint.

The original calculator is programmed in pearl/html, and the user must click a submit button to see the
calculation results. Our calculator, on the other hand, is implemented using FLASK and AJAX. This allows
the allows the calculations to be made and updated "live" on the page; no button-clicking/form-submission 
is necessary.

#Rules
The rules for the algorithms used in these calculations can be found in complete detail at
https://rusa.org/pages/acp-brevet-control-times-calculator, but here are a few guidelines:
--The user cannot enter a distance greater than maximum brevet distance.
--No negative numbers may be entered (these are automatically filtered with a couple lines of html code).
--A distance set at 0 will have a close time set 1 hour ahead of the start time.

