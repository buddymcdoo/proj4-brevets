from acp_times import open_time, close_time

import nose
import logging

logging.basicConfig(format="%(levelname)s:%(message)s",
                    level = logging.WARNING)
log = logging.getLogger(__name__)

def test_open_time_formatting():#Tests that the proper iso-formatted date string is returned.
     assert str(open_time(0, 200, "2020-01-01 10:10:00")) == "2020-01-01T10:10:00+00:00"

def test_close_time_formatting():#When 0 is entered for the control distance in close time, it should return 1 hour ahead of the starting time.
     assert str(close_time(0, 200, "2020-01-01 10:10:00")) == "2020-01-01T11:10:00+00:00"

def test_open_time_calculation(): #Tests open_time calculations based off of online brevet calculator.
     assert str(open_time(25, 200, "2020-01-01 12:00:00")) == "2020-01-01T12:44:00+00:00"

def test_close_time_calculation(): #Tests close_time calculations based off of online brevet calculator.
     assert str(close_time(25, 200, "2020-01-01 12:00:00")) == "2020-01-01T14:15:00+00:00"

def test_larger_distance_time_adjustment():#This is to test that the time calculation is adjusted properly for larger brevet controle distances.
     assert str(close_time(300, 400, "2020-01-01 12:00:00")) == "2020-01-02T08:00:00+00:00"
