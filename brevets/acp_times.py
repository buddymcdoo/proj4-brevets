"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow
import logging

logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.INFO)
log = logging.getLogger(__name__)

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    minTime = get_max_speed_time(control_dist_km)#get_max_speed_time(control_dist_km) #This is the maximum speed
    arrowController = arrow.get(brevet_start_time, 'YYYY-MM-DD HH:mm:ss')

    #Adjust times accordingly with adding minTime
    seconds = arrowController.second + int(minTime * 3600)
    minutes = arrowController.minute + get_num_divides(seconds, 60)
    seconds = seconds % 60
    hours = arrowController.hour + get_num_divides(minutes, 60)
    minutes = minutes % 60
    days = arrowController.day + get_num_divides(hours, 24)
    hours = hours % 24
    if(seconds > 59):
       minutes  = minutes + 1
       seconds = 0
    if(minutes > 59):
       hours = hours + 1
       minutes = 0
    if(hours > 23):
       days = days + 1
       hours = 0

    months = arrowController.month
    if(arrowController.month == 1 or arrowController.month == 3 or arrowController.month == 5 or arrowController.month == 7 or arrowController.month == 8 or arrowController.month == 10 or arrowController.month == 12):#months that have 31 days
       if(days > 31):
          months = months + 1
          days = day % 31
    elif(arrowController.month == 2):#special case for February
       if(arrowController.year % 4 == 0): #leap years
          if(days > 29):
             months = months + 1
             days = days % 29
       else:
          if(days > 28):
             months = months + 1
             days = days % 28
    else:
       if(days > 30):
          months = months + 1
          days = days % 30

    years = arrowController.year
    if(months > 12):
       years = years + 1
       months = 1

    arrowController = arrowController.replace(year = years, month = months, day = days, hour = hours, minute = minutes, second = 0)
    log.info("Testing: " + arrowController.isoformat())

    return arrowController.isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    maxTime = get_min_speed_time(control_dist_km)#This is the minimum speed
    arrowController = arrow.get(brevet_start_time, 'YYYY-MM-DD HH:mm:ss')

    if(control_dist_km == 0): #Closing time for dist 0 should always be 1 hour after opening time for dist 0
       arrowController = arrowController.replace(hour = (arrowController.hour + 1))
       return arrowController.isoformat()

    #Adjust times accordingly with adding maxTime
    seconds = arrowController.second + int(maxTime * 3600)
    minutes = arrowController.minute + get_num_divides(seconds, 60)
    seconds = seconds % 60
    hours = arrowController.hour + get_num_divides(minutes, 60)
    minutes = minutes % 60
    days = arrowController.day + get_num_divides(hours, 24)
    hours = hours % 24
    if(seconds > 59):
       minutes  = minutes + 1
       seconds = 0
    if(minutes > 59):
       hours = hours + 1
       minutes = 0
    if(hours > 23):
       days = days + 1
       hours = 0

    months = arrowController.month
    if(arrowController.month == 1 or arrowController.month == 3 or arrowController.month == 5 or arrowController.month == 7 or arrowController.month == 8 or arrowController.month == 10 or arrowController.month == 12):#months that have 31 days
       if(days > 31):
          months = months + 1
          days = days % 31
    elif(arrowController.month == 2):#special case for February
       if(arrowController.year % 4 == 0): #leap years
          if(days > 29):
             months = months + 1
             days = days % 29
       else:
          if(days > 28):
             months = months + 1
             days = days % 28
    else:
       if(days > 30):
          months = months + 1
          days = days % 30

    years = arrowController.year
    if(months > 12):
       years = years + 1
       months = 1

    arrowController = arrowController.replace(year = years, month = months, day = days, hour = hours, minute = minutes, second = 0)

    return arrowController.isoformat()


def get_num_divides(numerator, denominator): #Checks how many times a numerator divides into a denominator. Returns int val.
    num = numerator - (numerator % denominator)
    den = denominator
    times_divided = 0
    while(num > 0):
       num = num - den
       times_divided = times_divided + 1

    return times_divided


def get_min_speed_time(distance):
    sets_of_100 = get_num_divides(distance, 100)
    remainder = distance % 100
    distance_counter = 0
    time_to_add = 0

    #add time relative to each distance set (rules found at https://rusa.org/pages/acp-brevet-control-times-calculator)
    i = 0
    while(i < sets_of_100):
       distance_counter += 100
       if(distance_counter <= 600):
          time_to_add += 100/15
       elif(distance_counter <= 1000):
          time_to_add += 100/11.428
       else:
          time_to_add += 100/13.333
       i += 1

    #adds remainder
    if(distance_counter <= 60):
      time_to_add += (remainder/20 + 1)#Speciel rule here. See rules at website listed above.
    elif(distance_counter <= 600):
       time_to_add += remainder/15
    elif(distance_counter <= 1000):
       time_to_add += remainder/11.428
    else:
       time_to_add += remainder/13.333

    return time_to_add


def get_max_speed_time(distance):
    sets_of_100 = get_num_divides(distance, 100)
    remainder = distance % 100
    distance_counter = 0
    time_to_add = 0

    #add time relative to each distance set (rules found at https://rusa.org/pages/acp-brevet-control-times-calculator)
    i = 0
    while(i < sets_of_100):
       distance_counter += 100
       if(distance_counter <= 200):
          time_to_add += 100/34
       elif(distance_counter <= 400):
          time_to_add += 100/32
       elif(distance_counter <= 600):
          time_to_add += 100/30
       elif(distance_counter <= 1000):
          time_to_add += 100/28
       else:
          time_to_add += 100/26
       i += 1

    #adds remainder
    if(distance_counter <= 200):
       time_to_add += remainder/34
    elif(distance_counter <= 400):
       time_to_add += remainder/32
    elif(distance_counter <= 600):
       time_to_add += remainder/30
    elif(distance_counter <= 1000):
       time_to_add += remainder/28
    else:
       time_to_add += remainder/26

    return time_to_add